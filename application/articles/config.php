<?php

return [

    // 视图输出字符串内容替换,留空则会自动进行计算
    'view_replace_str'       => [
        '__ZUI__'       => '/static/common/lib/zui-1.9.0',
        '__SWIPER__'    => '/static/common/lib/Swiper-3.4.2',
        '__COMMON__'    => '/static/common',
        '__LIB__'       => '/static/common/lib',
        '__JS__'    	=> '/static/articles/js',
        '__IMG__'       => '/static/articles/images',
        '__CSS__'       => '/static/articles/css',   
    ],
];